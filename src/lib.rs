pub mod types;

use image::{imageops::FilterType, DynamicImage, GenericImage, GenericImageView, Pixel, RgbaImage};
pub use stable_eyre::eyre::{Result, WrapErr};
pub use std::fs;
use std::{collections::HashMap, path::PathBuf};
use types::Color;

pub fn crop_to_square(img: &DynamicImage) -> DynamicImage {
    let (w, h) = img.dimensions();
    let shortest = match w < h {
        true => w,
        false => h,
    };

    img.resize_exact(shortest, shortest, FilterType::CatmullRom)
}

pub fn crop_source_images(source_dir: &str, cropped_path: &str) -> Result<()> {
    // get an iterator over each entry in the source_dir
    let source_dir_entries = fs::read_dir(source_dir)
        .wrap_err_with(|| format!("Failed to access directory at: '{}'", source_dir))?;

    for (i, entry) in source_dir_entries.enumerate() {
        // chech whether the path can be read
        let path = entry
            .wrap_err("Failed to access entries in source directory")?
            .path();
        // attempt to open an image at the path of each given entry
        let img = image::open(&path)
            .wrap_err_with(|| format!("Failed to open image at: '{:?}'", &path))?;
        // crop the image to a square
        let img = crop_to_square(&img);
        // save created images into the cropped_sources directory
        img.save(format!("{}{}.jpg", cropped_path, i))
            .wrap_err_with(|| format!("Failed to save an image in: {}", cropped_path))?;
    }

    Ok(())
}

pub fn get_avg_color(img: &RgbaImage) -> Color {
    let mut red: f64 = 0.0;
    let mut green: f64 = 0.0;
    let mut blue: f64 = 0.0;
    let mut total_pix: f64 = 0.0;

    for p in img.pixels() {
        red += p.channels()[0] as f64;
        green += p.channels()[1] as f64;
        blue += p.channels()[2] as f64;
        total_pix += 1.0;
    }

    red /= total_pix;
    green /= total_pix;
    blue /= total_pix;

    let red = red.floor() as u8;
    let green = green.floor() as u8;
    let blue = blue.floor() as u8;

    Color { red, green, blue }
}

pub fn get_cropped_sources_colors(cropped_path: &str) -> Result<HashMap<PathBuf, Color>> {
    // start by checking that we are able to read the directory of
    // cropped sources (returns a Result<ReadDir>)
    fs::read_dir(cropped_path)
        // if we can't read the dir, handle the error
        .wrap_err_with(|| format!("Failed to access directory at: {}", cropped_path))?
        // ReadDir is an iterator, so for every Result<DirEntry> it produces,
        // we are going to transform it into something else
        .map(|entry| -> Result<(PathBuf, Color)> {
            // for each entry (Result<DirEntry>), we handle the error if
            // there is one, then use DirEntry.path() to give a PathBuf
            // which image::open() is able to take as an argument
            let path = entry
                .wrap_err("Failed to access entries in source directory")?
                .path();
            // for each path, we try to open the image with the path of the
            // supplied DirEntry.
            let img = image::open(&path)
                // even if the path for the image to open is valid, there's still the
                // possibility that the image can't be opened by the image library,
                // so handle that error
                .wrap_err_with(|| format!("Failed to open image at: '{:?}'", &path))?;
            // if we made it through all of this, img will be the type DynamicImage,
            // which we can give to avg_color to give a Color struct. this is then
            // wrapped with an Ok() to satisfy the Result of the closure
            Ok((path, get_avg_color(&img.into_rgba8())))
        })
        // what this accomplishes is transforming a ReadDir iterator into an iterator
        // of Result<Color>. the collect method is amazing, and we are able to collect
        // into a single Result<Vec<Color>>, bypassing the need to check every
        // Result<Color> and giving a single Ok(Vec) or Err(e)
        .collect::<Result<HashMap<PathBuf, Color>>>()
}

pub fn get_cropped_sources_thumbnails(
    cropped_path: &str,
    tile_size: &u32,
) -> Result<HashMap<PathBuf, DynamicImage>> {
    fs::read_dir(cropped_path)
        .wrap_err_with(|| format!("Failed to access directory at: {}", cropped_path))?
        .map(|entry| -> Result<(PathBuf, DynamicImage)> {
            let path = entry
                .wrap_err("Failed to access entries in source directory")?
                .path();
            let img = image::open(&path)
                .wrap_err_with(|| format!("Failed to open image at: '{:?}'", &path))?;
            let img = img.thumbnail_exact(*tile_size, *tile_size);
            Ok((path, img))
        })
        .collect::<Result<HashMap<PathBuf, DynamicImage>>>()
}

fn get_best_match(tile_avg_color: &Color, cropped_colors: &HashMap<PathBuf, Color>) -> PathBuf {
    let mut best_name = PathBuf::new();
    let mut best_color_diff: f64 = -1.0;

    for (path, color) in cropped_colors {
        let diff = tile_avg_color.diff(color);
        if best_color_diff < 0.0 || diff < best_color_diff {
            best_name = path.clone();
            best_color_diff = diff;
        }
    }

    best_name
}

pub fn run(
    input_path: &str,
    output_path: &str,
    cropped_path: &str,
    source_dir: &str,
    generate: bool,
    tile_size: Option<&str>,
) -> Result<()> {
    // make sure we can open the given input image
    let input_image = image::open(input_path)
        .wrap_err_with(|| format!("Failed to open image at: '{}'", input_path))?;

    // make sure that the tile size is a valid number
    let tile_size = match tile_size {
        Some(a) => a.parse::<u32>().wrap_err_with(|| {
            format!("Invalid tile size: {} is not a whole, positive integer", a)
        })?,
        None => 15,
    };

    // check to see if the cropped_sources directory exists, if it doesn't create it
    // and generate cropped sources
    if let Err(_) = fs::read_dir(cropped_path) {
        fs::create_dir(cropped_path)
            .wrap_err_with(|| format!("Failed to create {} directory", cropped_path))?;
        println!("Generating new cropped sources");
        crop_source_images(source_dir, cropped_path).wrap_err("Failed to crop source images")?;
    }

    // at this point cropped_sources exists, so check to see if we should
    // generate new cropped sources
    if let true = generate {
        println!("Generating new cropped sources");
        crop_source_images(source_dir, cropped_path)
            .wrap_err_with(|| format!("Failed to create {} directory", cropped_path))?;
    }

    // get a hashmap with filepaths as keys and the average color of
    // each cropped source image as a value
    let cropped_colors = get_cropped_sources_colors(cropped_path)
        .wrap_err("Failed to get average colors of cropped images")?;

    // make each cropped image the same size and load them into a hashmap
    // as the values with their filepaths as the keys
    let cropped_thumbnails = get_cropped_sources_thumbnails(cropped_path, &tile_size)
        .wrap_err("Failed to get thumbnails of cropped images")?;

    let (target_width, target_height) = input_image.dimensions();

    let mut output_image: RgbaImage = RgbaImage::new(target_width, target_height);

    for x in (0..target_width - tile_size).step_by(tile_size as usize) {
        for y in (0..target_height - tile_size).step_by(tile_size as usize) {
            println!("{}, {}", x, y);
            let tile = input_image.view(x, y, tile_size, tile_size).to_image();
            let tile_avg_color = get_avg_color(&tile);
            let best_thumbnail_path = get_best_match(&tile_avg_color, &cropped_colors);
            let best_thumbnail = match cropped_thumbnails.get(&best_thumbnail_path) {
                Some(a) => a,
                _ => unreachable!(),
            };
            output_image
                .copy_from(best_thumbnail, x, y)
                .wrap_err("Failed to copy content from thumbnail images into output image")?;
        }
    }

    output_image
        .save(output_path)
        .wrap_err_with(|| format!("Failed to save output at: {}", output_path))?;

    Ok(())
}

#[cfg(test)]
mod tests {

    use image::Rgba;

    use super::*;

    #[test]
    fn crop_image() {
        let img = image::open("test.jpg").unwrap();
        let (w, h) = img.dimensions();
        let dimension = match w < h {
            true => w,
            false => h,
        };
        let img = crop_to_square(&img);
        assert_eq!(img.dimensions(), (dimension, dimension));
    }

    #[test]
    fn average_color() {
        let mut img = DynamicImage::new_rgb8(100, 100);
        let start = Rgba::from_slice(&[255, 0, 255, 255]);
        let end = Rgba::from_slice(&[255, 0, 255, 255]);
        image::imageops::vertical_gradient(&mut img, start, end);
        let c = get_avg_color(&img.into_rgba8());
        println!("{:?}", c);
        assert_eq!(c.red, 255);
        assert_eq!(c.green, 0);
        assert_eq!(c.blue, 255);
    }

    #[test]
    fn best_match() {
        let test_color = Color {
            red: (255),
            green: (255),
            blue: (255),
        };
        let mut test_map = HashMap::new();
        test_map.insert(
            PathBuf::from("path1"),
            Color {
                red: (255),
                green: (255),
                blue: (255),
            },
        );
        test_map.insert(
            PathBuf::from("path2"),
            Color {
                red: (128),
                green: (128),
                blue: (128),
            },
        );
        test_map.insert(
            PathBuf::from("path3"),
            Color {
                red: (0),
                green: (128),
                blue: (255),
            },
        );
        assert_eq!(
            PathBuf::from("path1"),
            get_best_match(&test_color, &test_map)
        );
    }
}
